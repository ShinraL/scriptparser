﻿using ShenGu.Script;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptTest
{
    public class SimpleScript : IExecutor
    {
        public string ScriptContent
        {
            get
            {
                return @"
var a = { name: 'test', age: 25};
for (var i = 0; i < 3; i++)
    alert(a.name);

if (a.age < 18) alert(a.name + '是小孩子');
else if (a.age < 40) alert(a.name + '是年青人');
else alert(a.name + '是老年人');

switch(a.name) {
    case 'test': alert('这是一个测试用户'); break;
    default: alert('正式用户！');
}

alert(status);
";
            }
        }

        public Type[] ScriptTypes { get { return null; } }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["status"] = "已完成";
                return result;
            }
        }
    }

    [ScriptMapping("User")]
    public class User
    {
        [ObjectMember]
        public string Name { get; set; }

        [ObjectMember]
        public int Age { get; set; }

        [ObjectMember]
        public string Check(string operate)
        {
            return string.Format("用户（用户名：{0}，年龄：{1}）正在{2}", Name, Age, operate);
        }
    }

    public class TypeMappingExecutor : IExecutor
    {
        public string ScriptContent
        {
            get
            {
                return @"
var u = new User();
u.Name = 'tester';
u.Age = 25;
var ttt = u.Check('写代码');
alert(ttt);
";
            }
        }

        public Type[] ScriptTypes { get { return new Type[] { typeof(User) }; } }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["User"] = typeof(User);
                return result;
            }
        }
    }

    [ScriptProxy(typeof(Form))]
    public class FormProxy : IScriptProxy
    {
        private Form form;

        public object RealInstance
        {
            get { return form; }
            set { form = (Form)value; }
        }

        [ObjectMember]
        public string Title
        {
            get { return form.Text; }
            set { form.Text = value; }
        }

        [ObjectMember]
        public void Show()
        {
            form.Show();
        }
        
        [ObjectMember]
        public void ShowDialog()
        {
            form.ShowDialog();
        }
    }

    public class TypeProxyExecutor : IExecutor
    {
        public string ScriptContent
        {
            get
            {
                return @"
var f1 = new Form();
f1.Title = '这是一个测试窗体。';
f1.Show();

var f2 = new Form();
f2.Title = '这是一个模式窗体。。。。。';
f2.ShowDialog();
";
            }
        }

        public Type[] ScriptTypes
        {
            get
            {
                return new Type[] { typeof(FormProxy) };
            }
        }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["Form"] = typeof(Form);
                return result;
            }
        }
    }

    public class MethodMappingExecutor : IExecutor
    {
        public string ScriptContent
        {
            get
            {
                return @"
var users = QueryUsers(1);
for (var i in users.rows) {
    alert(stringify(users.rows[i]));
}
";
            }
        }

        [ScriptMapping]
        public DataTable QueryUsers(int type)
        {
            DataTable table = new DataTable("Users");
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("UserName", typeof(string));
            table.Columns.Add("Age", typeof(int));
            table.Rows.Add("Admin", "管理员", 25);
            table.Rows.Add("Tester", "测试人员", 24);
            return table;
        }

        public Type[] ScriptTypes
        {
            get
            {
                return null;
            }
        }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                return null;
            }
        }
    }

    public static class DefaultScripts
    {
        #region 模板
        public const string Template = @"
<html>
	<head></head>
	<body>
		<p>
			<h3>if语法</h3>
			状态：
			<@if code='status==0'>未开始</@if>
			<@else if='status==1'>已进场</@else>
			<@else if='status==2'>已出场</@else>
			<@else>异常离场</@else>
		</p>
		<p>
			<h3>switch语法</h3>
            状态：
			<span style = 'color:red'>
                <@switch code='status'>
					<@case code='0'>
                        未开始
                    </@case>
                    <@case code='1'>
						已进场
                    </@case>
					<@case code='2'>
                        已出场
                    </@case>
                    <@default>
                        异常离场
                    </@default>
                </@switch>
            </span>
        </p>
        <p>
            <h3>for语法</h3>
            <ul>
                <@for code='var i = 0; i < users.rows.count; i++'>
					<@line code = 'var row = users.rows[i]' />
                    <li>
                        编号：@{row.userId}，名称：@{row.userName}，年龄：@{row.age}
					</li>
				</@for>
			</ul>
		</p>
		<p>
			<h3>for...in语法</h3>
			<table>
				<tr>
					<td>用户编号</td>
					<td>用户名称</td>
					<td>用户年龄</td>
				</tr>
				<@for code = 'var i in users.rows' >

                <@line code='var row = users.rows[i]'/>
				<tr>
					<td>@{row.userid}</td>
					<td>@{row.username}</td>
					<td>@{row.age}</td>
				</tr>
				</@for>
			</table>
		</p>
		<@code>
			var books = [
				{ no: '1001', name: '格林童话', price: 19.8 },
				{ no: '1002', name: '安徒生童话', price: 29.8 },
				{ no: '1003', name: '.Net高级编程', price: 101.5 }
			];
		</@code>
		<p>
			<h3>while语法</h3>
			<table>
				<tr>
					<td>编号</td>
					<td>名称</td>
					<td>价格</td>
				</tr>
				<@line code = 'var i = 0' />
                <@while code='i < books.length'>
				<tr>
					<td>@{books[i].no}</td>
					<td>@{books[i].name}</td>
					<td>@{books[i].price}</td>
				</tr>
				<@line code = 'i++' />
                </@while>
            </table>
        </p>
        <p>
            <h3>do...while语法</h3>
			<table>
				<tr>
					<td>编号</td>
					<td>名称</td>
					<td>价格</td>
				</tr>
				<@line code = 'var i = 0' />

                <@do while='i < books.length'>
				<tr>
					<td>@{books[i].no}</td>
					<td>@{books[i].name}</td>
					<td>@{books[i].price}</td>
				</tr>
				<@line code = 'i++' />
                </@do >
            </table>
        </p>
    </body>
</html>
";
        #endregion
    }
}
