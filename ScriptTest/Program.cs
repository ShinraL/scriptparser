﻿using System;
using System.Windows.Forms;

namespace ScriptTest
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new MainForm());
        }
    }
}
