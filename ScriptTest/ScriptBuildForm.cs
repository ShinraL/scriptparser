﻿using ShenGu.Script;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptTest
{
    public partial class ScriptBuildForm : Form
    {
        public ScriptBuildForm(string script)
        {
            InitializeComponent();
            if (!string.IsNullOrEmpty(script)) txtContent.Text = script;
        }

        private DataTable CreateUserTable()
        {
            DataTable table = new DataTable("Users");
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("UserName", typeof(string));
            table.Columns.Add("Age", typeof(int));
            table.Rows.Add("Admin", "管理员", 25);
            table.Rows.Add("Tester", "测试人员", 24);
            return table;
        }

        private void btnBuild_Click(object sender, EventArgs e)
        {
            try
            {
                string text = txtContent.Text;

                //将包含<@XXX></@XXX>的标签，转成可以执行的javascript语法
                TagParser reader = new TagParser();
                string script = reader.Parse(text, "output", "write", true);

                //解析javascript语法
                ScriptParser parser = ScriptParser.Parse(script);

                ScriptContext context = new ScriptContext();
                //添加output和write的方法映射
                ScriptResult result = new ScriptResult(text);
                context.AddMappings(result);

                //添加其它的变量
                context.AddValue("users", CreateUserTable());
                context.AddValue("status", 3);
                
                //执行javascript脚本
                parser.Execute(context);
                
                //读取结果
                string content = result.ToString();

                using (ScriptBuildResultForm form = new ScriptBuildResultForm(script, content))
                    form.ShowDialog();
            }
            catch(Exception ex)
            {
                MessageBox.Show("出现异常：" + ex.Message);
            }
        }
    }

    class ScriptResult
    {
        private string source;
        private StringBuilder builder;

        public ScriptResult(string source)
        {
            this.source = source;
            this.builder = new StringBuilder();
        }

        [ScriptMapping("output")]
        public void Output(int index, int length)
        {
            builder.Append(source.Substring(index, length));
        }

        [ScriptMapping("write")]
        public void Write(IScriptObject value, ScriptContext context)
        {
            builder.Append(value.ToValueString(context));
        }

        public override string ToString()
        {
            return builder.ToString();
        }
    }
}
