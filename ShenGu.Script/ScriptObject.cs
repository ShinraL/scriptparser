﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;

namespace ShenGu.Script
{
    internal class ScriptFieldInfo
    {
        internal readonly ScriptContext Context;
        internal readonly string FieldName;
        internal readonly bool IsFuncContext;
        internal int FieldIndex;
        internal long ObjectId;
        internal int ParentLevel;

        public ScriptFieldInfo(ScriptContext context, bool isFuncContext, string fieldName)
        {
            this.Context = context;
            this.IsFuncContext = isFuncContext;
            this.FieldName = fieldName;
        }

        public long NewObjectId()
        {
            return Context.NewObjectId();
        }
    }

    public interface IScriptEnumerable
    {
        IEnumerator GetEnumerator(ScriptContext context, bool isKey);
    }

    public interface IScriptObject : IScriptEnumerable
    {
        IScriptObject GetValue(ScriptContext context, string name);

        void SetValue(ScriptContext context, string name, IScriptObject value);

        bool Remove(ScriptContext context, string name);

        /// <summary>转成具体的值</summary>
        object ToValue(ScriptContext context);

        /// <summary>获取值的字符串显示</summary>
        string ToValueString(ScriptContext context);

        string TypeName { get; }
    }

    public abstract class ScriptObjectBase : HashEntryList<IScriptObject>, IScriptObject
    {
        private int objectIdFlag;   // 1-新增时重置，2-删除时重置，4-使用内部成员列表的ObjectId
        private bool resetIfDeleted;
        private long objectId;
        private bool reloadSysMembers;
        private IScriptMemberList systemMembers, valueMembers;
        private IScriptObject prototype;
        internal IScriptObject Parent;

        protected ScriptObjectBase() : this(true, true) { }

        protected ScriptObjectBase(bool useSystemMembers) : this(useSystemMembers, useSystemMembers)
        {
        }

        protected ScriptObjectBase(bool useSystemMembers, bool useMemberObjectId)
        {
            this.reloadSysMembers = useSystemMembers;
            if (useMemberObjectId) this.objectIdFlag = 4;
        }

        private void CheckSystemMembers(ScriptContext context)
        {
            if (reloadSysMembers)
            {
                systemMembers = ScriptGlobal.GetInstanceMembers(context, this.GetType());
                reloadSysMembers = false;
            }
        }

        internal void InitSystemMembers(IScriptMemberList members)
        {
            this.systemMembers = members;
            this.reloadSysMembers = false;
        }

        protected void InitValueMembers(IScriptMemberList members)
        {
            this.valueMembers = members;
        }

        internal virtual bool IsFuncContext { get { return false; } }

        internal long ObjectId { get { return objectId; } set { objectId = value; } }

        private ScriptObjectBase FindFieldInfo(ScriptFieldInfo fieldInfo)
        {
            ScriptObjectBase result = this;
            int parentLevel = 0;
            bool resetObjectId = this.objectId <= 0;
            if (fieldInfo.ObjectId > 0 && fieldInfo.ObjectId == this.objectId)
            {
                if (fieldInfo.ParentLevel == 0) return result;
                else
                {
                    int lastParentLevel = fieldInfo.ParentLevel;
                    for (int i = 0; i < lastParentLevel; i++)
                    {
                        result = (ScriptObjectBase)result.Parent;
                        if (result.objectId <= 0 || result.objectId > this.objectId)
                        {
                            parentLevel = i + 1;
                            resetObjectId = true;
                            break;
                        }
                    }
                    if (parentLevel == 0)
                        return result;
                }
            }
            int index;
            do
            {
                if (result != this && result.objectId <= 0)
                {
                    if (!result.IsReadOnly)
                        result.ResetObjectId(fieldInfo, false);
                    resetObjectId = true;
                }
                else if (!resetObjectId && result.objectId > this.objectId)
                    resetObjectId = true;
                index = result.InnerFind(fieldInfo.Context, fieldInfo.FieldName);
                if (index >= 0) break;
                else
                {
                    IScriptObject p2 = result.Parent;
                    ScriptObjectBase p = p2 as ScriptObjectBase;
                    if (p != null)
                    {
                        if (!result.IsReadOnly) result.objectIdFlag |= 1;
                        parentLevel++;
                        result = p;
                    }
                    else
                    {
                        if (p2 == null) index = -1;
                        else index = -2;
                        break;
                    }
                }
            } while (true);
            if (!result.IsReadOnly)
                result.objectIdFlag |= (index < 0 ? 1 : 2);
            if (resetObjectId && !this.IsReadOnly) this.ResetObjectId(fieldInfo, result != this);
            fieldInfo.FieldIndex = index;
            fieldInfo.ParentLevel = parentLevel;
            fieldInfo.ObjectId = this.objectId;
            return result;
        }

        private void ResetObjectId(ScriptFieldInfo fieldInfo, bool forceRenew)
        {
            if (!forceRenew && (this.objectIdFlag & 4) != 0)
            {
                IScriptMemberList memberList = valueMembers;
                if (memberList == null)
                {
                    CheckSystemMembers(fieldInfo.Context);
                    memberList = systemMembers;
                }
                if (memberList != null)
                {
                    this.objectId = memberList.ObjectId;
                    if (this.objectId <= 0)
                        memberList.ObjectId = this.objectId = fieldInfo.NewObjectId();
                    this.objectIdFlag = 1;
                    return;
                }
            }
            this.objectId = fieldInfo.NewObjectId();
            this.objectIdFlag = 0;
        }

        internal IScriptObject GetValue(ScriptFieldInfo fieldInfo)
        {
            ScriptObjectBase obj = FindFieldInfo(fieldInfo);
            if (fieldInfo.FieldIndex == -1)
            {
                if (fieldInfo.IsFuncContext)
                    throw new ScriptExecuteException(string.Format("全局变量'{0}'未定义。", fieldInfo.FieldName));
                return ScriptUndefined.Instance;
            }
            if (fieldInfo.FieldIndex == -2)
                return obj.Parent.GetValue(fieldInfo.Context, fieldInfo.FieldName);
            return obj.InnerGetValue(fieldInfo.Context, fieldInfo.FieldIndex);
        }

        internal void SetValue(ScriptFieldInfo fieldInfo, IScriptObject value)
        {
            if (!IsReadOnly)
            {
                if (fieldInfo.IsFuncContext)
                {
                    ScriptObjectBase obj = FindFieldInfo(fieldInfo);
                    switch (fieldInfo.FieldIndex)
                    {
                        case -1: fieldInfo.FieldIndex = obj.InnerSetValue(fieldInfo.Context, fieldInfo.FieldName, value); break;
                        case -2: obj.Parent.SetValue(fieldInfo.Context, fieldInfo.FieldName, value); break;
                        default:
                            {
                                if (!obj.InnerSetValue(fieldInfo.Context, fieldInfo.FieldIndex, value))
                                    fieldInfo.FieldIndex = ~obj.InnerSetValue(fieldInfo.Context, fieldInfo.FieldName, value);
                            }
                            break;
                    }
                }
                else
                {
                    if (fieldInfo.ObjectId > 0 && fieldInfo.ObjectId == this.objectId)
                        this.InnerSetValue(fieldInfo.Context, fieldInfo.FieldIndex, value);
                    else
                    {
                        int index = this.InnerFind(fieldInfo.Context, fieldInfo.FieldName);
                        if (index < 0 || !this.InnerSetValue(fieldInfo.Context, index, value))
                            index = ~this.InnerSetValue(fieldInfo.Context, fieldInfo.FieldName, value);
                        if (this.objectId <= 0) this.objectId = fieldInfo.NewObjectId();
                        fieldInfo.ObjectId = this.objectId;
                        fieldInfo.ParentLevel = 0;
                        fieldInfo.FieldIndex = index;
                    }
                }
            }
        }

        #region 重载方法

        protected virtual int OnFindSystemMember(ScriptContext context, string key)
        {
            return systemMembers.Find(context, key);
        }

        protected override int InnerFind(ScriptContext context, string key)
        {
            CheckSystemMembers(context);
            int index = base.InnerFind(context, key);
            if (index >= 0)
            {
                if (systemMembers != null) index += systemMembers.Count;
                if (valueMembers != null) index += valueMembers.Count;
                return index;
            }
            if (valueMembers != null)
            {
                index = valueMembers.Find(context, key);
                if (index >= 0)
                {
                    if (systemMembers != null) index += systemMembers.Count;
                    return index;
                }
            }
            if (systemMembers != null)
            {
                index = OnFindSystemMember(context, key);
                if (index >= 0) return index;
            }
            return index;
        }

        protected override IScriptObject InnerGetValue(ScriptContext context, int index)
        {
            if (index < 0) return ScriptUndefined.Instance;
            CheckSystemMembers(context);
            int count = 0, c2, i2;
            if (systemMembers != null)
            {
                c2 = systemMembers.Count;
                if (index < c2) return systemMembers.GetValue(context, this, index);
                count = c2;
            }
            if (valueMembers != null)
            {
                c2 = valueMembers.Count;
                i2 = index - count;
                if (i2 < c2) return valueMembers.GetValue(context, this, i2);
                count += c2;
            }
            c2 = this.Count;
            i2 = index - count;
            if (i2 < c2) return base.InnerGetValue(context, i2);
            return ScriptUndefined.Instance;
        }

        protected override bool InnerSetValue(ScriptContext context, int index, IScriptObject value)
        {
            int count = 0, c2, index2;
            CheckSystemMembers(context);
            if (systemMembers != null)
            {
                c2 = systemMembers.Count;
                if (index < c2)
                    return systemMembers.SetValue(context, this, index, value);
                count = c2;
            }
            if (valueMembers != null)
            {
                c2 = valueMembers.Count;
                index2 = index - count;
                if (index2 < c2)
                    return valueMembers.SetValue(context, this, index2, value);
                count += c2;
            }
            c2 = this.Count;
            index2 = index - count;
            return base.InnerSetValue(context, index2, value);
        }

        protected override int InnerSetValue(ScriptContext context, string key, IScriptObject value)
        {
            int index = InnerFind(context, key);
            if (index >= 0)
            {
                int c1 = systemMembers == null ? 0 : systemMembers.Count, c2 = valueMembers == null ? 0 : valueMembers.Count;
                if (index < c1)
                {
                    if (systemMembers.SetValue(context, this, index, value)) return index;
                }
                else
                {
                    int index2 = index - c1;
                    if (index2 < c2)
                    {
                        if (valueMembers.SetValue(context, this, index2, value)) return index;
                    }
                    else
                    {
                        base.InnerSetValue(context, index2 - c2, value);
                        return index;
                    }
                }
            }
            index = base.InnerSetValue(context, key, value);
            int count = 0;
            CheckSystemMembers(context);
            if (systemMembers != null) count += systemMembers.Count;
            if (valueMembers != null) count += valueMembers.Count;
            if (count > 0)
            {
                bool navigate = index < 0;
                if (navigate) index = ~index;
                index += count;
                if (navigate) index = ~index;
            }
            return index;
        }

        internal int BaseSetValue(ScriptContext context, string key, IScriptObject value)
        {
            return base.InnerSetValue(context, key, value);
        }

        protected override void OnAdded(string key, IScriptObject newValue)
        {
            base.OnAdded(key, newValue);
            if ((this.objectIdFlag & 1) != 0)
            {
                this.objectId = 0;
                this.objectIdFlag = 0;
            }
        }

        protected override void OnRemoved(string key, IScriptObject oldValue)
        {
            base.OnRemoved(key, oldValue);
            if ((this.objectIdFlag & 2) != 0)
            {
                this.objectId = 0;
                this.objectIdFlag = 0;
            }
        }

        #endregion

        #region 系统方法/属性

        [ObjectMember("prototype", IsEnumerable = false)]
        public IScriptObject ProtoType
        {
            get { return prototype == null ? this : prototype;}
            set { this.prototype = value; }
        }

        [ObjectMember("toString", IsEnumerable = false)]
        public virtual IScriptObject GetString(ScriptContext context)
        {
            string str = ToValueString(context);
            if (str == null) return ScriptNull.Instance;
            return new ScriptString(str);
        }

        #endregion

        #region IScriptObject

        public virtual bool Remove(ScriptContext context, string name)
        {
            return !IsReadOnly && InnerRemove(context, name);
        }

        public abstract object ToValue(ScriptContext context);

        public abstract string ToValueString(ScriptContext context);

        public override string ToString()
        {
            string str = ToValueString(null);
            int count = this.Count;
            if (count == 0) return str;
            StringBuilder sb = new StringBuilder();
            sb.Append(str);
            sb.Append(", {");
            int index = 0;
            foreach(KeyValuePair<string, IScriptObject> kv in (IEnumerable<KeyValuePair<string, IScriptObject>>)this)
            {
                sb.Append(kv.Key);
                sb.Append(':');
                sb.Append(kv.Value.ToValueString(null));
                sb.Append(',');
                if (++index > 5) break;
            }
            if (index == count)
                sb.Length--;
            else
                sb.Append("...");
            sb.Append('}');
            return sb.ToString();
        }

        public virtual IScriptObject GetValue(ScriptContext context, string name)
        {
            return InnerGetValue(context, name);
        }

        public virtual void SetValue(ScriptContext context, string name, IScriptObject value)
        {
            if (!IsReadOnly)
            {
                int index = InnerFind(context, name);
                if (index >= 0) InnerSetValue(context, index, value);
                else InnerSetValue(context, name, value);
            }
        }

        public virtual IEnumerator GetEnumerator(ScriptContext context, bool isKey)
        {
            return new ScriptEnumerator(context, isKey, this);
        }

        public virtual bool BooleanValue { get { return true; } }

        public virtual string TypeName { get { return "object"; } }

        #endregion

        #region 内部类

        struct ScriptEnumerator : IEnumerator
        {
            private ScriptContext context;
            private ScriptObjectBase instance;
            private IEnumerator<KeyValuePair<string, IScriptObject>> instanceValues;
            private IEnumerator<KeyValuePair<string, IScriptObject>> systemMemberValues, valueMemberValues;
            private int status;
            private bool isKey;
            private object current;

            public ScriptEnumerator(ScriptContext context, bool isKey, ScriptObjectBase instance)
            {
                this.context = context;
                this.isKey = isKey;
                this.instance = instance;
                this.instance.CheckSystemMembers(context);
                this.instanceValues = ((IEnumerable<KeyValuePair<string, IScriptObject>>)this.instance).GetEnumerator();
                this.systemMemberValues = this.instance.systemMembers != null ? this.instance.systemMembers.GetEnumerator() : null;
                this.valueMemberValues = this.instance.valueMembers != null ? this.instance.valueMembers.GetEnumerator() : null;
                this.status = -1;
                this.current = null;
            }

            public object Current
            {
                get
                {
                    if (status < 0)
                        throw new ArgumentOutOfRangeException("Current");
                    if (!isKey)
                    {
                        IScriptProperty propCurrent = current as IScriptProperty;
                        if (propCurrent != null && propCurrent is IScriptObject)
                            return ScriptHelper.CheckGetPropValue(context, instance, (IScriptObject)propCurrent);
                    }
                    return current;
                }
            }

            object IEnumerator.Current { get { return Current; } }

            public void Dispose()
            {
            }

            private bool CheckMoveNext(IEnumerator<KeyValuePair<string, IScriptObject>> list)
            {
                if (list != null)
                {
                    if (list.MoveNext())
                    {
                        KeyValuePair<string, IScriptObject> kv = list.Current;
                        do
                        {
                            IScriptValueEnumerable p = kv.Value as IScriptValueEnumerable;
                            if (p == null || p.IsEnumerable)
                            {
                                if (isKey) current = kv.Key;
                                else current = kv.Value;
                                return true;
                            }
                        } while (list.MoveNext());
                    }
                }
                return false;
            }

            public bool MoveNext()
            {
                if (status >= -1)
                {
                    if (CheckMoveNext(systemMemberValues))
                    {
                        status = 0;
                        return true;
                    }
                    if (CheckMoveNext(valueMemberValues))
                    {
                        status = 1;
                        return true;
                    }
                    if (CheckMoveNext(instanceValues))
                    {
                        status = 2;
                        return true;
                    }
                    status = -2;
                }
                return false;
            }

            public void Reset()
            {
                status = -1;
                if (systemMemberValues != null) systemMemberValues.Reset();
                if (valueMemberValues != null) valueMemberValues.Reset();
                if (instanceValues != null) instanceValues.Reset();
            }
        }

        #endregion
    }

    /// <summary></summary>
    public enum NumberType
    {
        NaN = -2,
        Infinity = -1,
        Integer = 1,
        Decimal = 2
    }

    public abstract class ScriptNumber : ScriptObjectBase
    {
        private const int Flag_Infinity = 1, Flag_NaN = 2;
        public readonly static ScriptNumber Infinity = new ScriptNumberConst(NumberType.Infinity);
        public readonly static ScriptNumber NaN = new ScriptNumberConst(NumberType.NaN);
        private long longValue;
        private decimal decimalValue;
        private readonly static ScriptInteger[] DefineValues = new ScriptInteger[101];

        internal ScriptNumber(bool useSystemMembers) : base(useSystemMembers) { }

        [ObjectConstructor]
        public static ScriptNumber Create(ScriptMethodArgus argus)
        {
            if (argus.Arguments != null && argus.Arguments.Length >0)
            {
                ScriptNumber num = argus.Arguments[0] as ScriptNumber;
                if (num != null) return num;
            }
            return Create(0);
        }

        public static ScriptNumber Create(long value)
        {
            ScriptInteger result;
            if (value >= 0 && value <= 100)
            {
                result = DefineValues[value];
                if (result == null)
                {
                    result = new ScriptInteger(value);
                    Interlocked.CompareExchange<ScriptInteger>(ref DefineValues[value], result, null);
                }
            }
            else
                result = new ScriptInteger(value);
            return result;
        }

        public static ScriptNumber Create(decimal value)
        {
            return new ScriptDecimal(value);
        }

        public abstract NumberType Type { get; }
        
        public abstract long IntegerValue { get; }
        public abstract decimal DecimalValue { get; }

        public override string TypeName { get { return "number"; } }

        private class ScriptNumberConst : ScriptNumber
        {
            private NumberType type;

            public ScriptNumberConst(NumberType type) : base(false) { this.type = type; }

            public override NumberType Type { get { return type; } }

            public override long IntegerValue
            {
                get { return long.MaxValue; }
            }

            public override decimal DecimalValue
            {
                get { return Decimal.MaxValue; }
            }

            public override object ToValue(ScriptContext context)
            {
                throw new ScriptExecuteException("不合法的数值：" + ToString());
            }

            public override bool BooleanValue { get { return type != NumberType.NaN; } }

            public override string ToValueString(ScriptContext context)
            {
                return type == NumberType.Infinity ? "Infinity" : "NaN";
            }
        }
    }

    public sealed class ScriptInteger : ScriptNumber
    {
        private long value;
        internal ScriptInteger(long value) : base(true) { this.value = value; }

        public override NumberType Type { get { return NumberType.Integer; } }

        public override long IntegerValue
        {
            get { return value; }
        }

        public override decimal DecimalValue
        {
            get { return (decimal)value; }
        }

        public static ScriptInteger GetInstance(long value) { return new ScriptInteger(value); }

        public override object ToValue(ScriptContext context) { return value; }

        public override bool BooleanValue { get { return value != 0; } }

        public override string ToValueString(ScriptContext context)
        {
            return value.ToString();
        }
    }

    public sealed class ScriptDecimal : ScriptNumber
    {
        private decimal value;

        internal ScriptDecimal(decimal value) : base(true) { this.value = value; }

        public override NumberType Type { get { return NumberType.Decimal; } }

        public override long IntegerValue
        {
            get { return (long)value; }
        }

        public override decimal DecimalValue
        {
            get { return value; }
        }

        public override object ToValue(ScriptContext context) { return value; }

        public override bool BooleanValue { get { return value != 0; } }

        public override string ToValueString(ScriptContext context) { return value.ToString(); }
    }

    public sealed class ScriptString : ScriptObjectBase
    {
        private string value;

        [ObjectConstructor]
        public ScriptString(string value) { this.value = value; }

        public string Value { get { return this.value; } }

        public override object ToValue(ScriptContext context) { return value; }

        public override string ToValueString(ScriptContext context) { return value; }

        [ObjectMember("toString")]
        public override IScriptObject GetString(ScriptContext context)
        {
            return this;
        }
    }

    public sealed class ScriptBoolean : ScriptObjectBase
    {
        private bool value;
        public readonly static ScriptBoolean True = new ScriptBoolean(true);
        public readonly static ScriptBoolean False = new ScriptBoolean(false);
        
        private ScriptBoolean(bool value) { this.value = value; }

        public bool Value { get { return value; } }

        [ObjectConstructor]
        public static ScriptBoolean Create(bool value)
        {
            return value ? True : False;
        }

        public override object ToValue(ScriptContext context) { return value; }

        public override bool BooleanValue { get { return value; } }

        public override string ToValueString(ScriptContext context) { return value ? "true" : "false"; }

        public override string TypeName { get { return "boolean"; } }
    }

    public sealed class ScriptDate : ScriptObjectBase
    {
        private DateTime date;

        private ScriptDate(DateTime date) { this.date = date; }
        public override bool BooleanValue { get { return true; } }

        public override string ToValueString(ScriptContext context) { return date.ToString("yyyy-MM-dd HH:mm:ss"); }

        public override string TypeName { get { return "object"; } }

        public override object ToValue(ScriptContext context)
        {
            return date;
        }

        #region 脚本方法/属性

        public static ScriptDate Create(DateTime date) { return new ScriptDate(date); }

        [ObjectConstructor]
        public static ScriptDate Create(ScriptMethodArgus info)
        {
            if (info.HasArguments)
            {
                IScriptObject value = info.Arguments[0];
                if (value is ScriptString)
                {
                    string str = ((ScriptString)value).Value;
                    return Create(DateTime.Parse(str));
                }
            }
            return Create(DateTime.Now);
        }

        #endregion
    }

    public sealed class ScriptUndefined : ScriptObjectBase
    {
        public readonly static ScriptUndefined Instance = new ScriptUndefined();

        private ScriptUndefined() : base(false) { }

        public override object ToValue(ScriptContext context) { return null; }

        public override bool BooleanValue { get { return false; } }

        public override string ToValueString(ScriptContext context) { return "undefined"; }

        public override string TypeName { get { return "undefined"; } }
    }

    public sealed class ScriptNull : ScriptObjectBase
    {
        public static ScriptNull Instance = new ScriptNull();

        private ScriptNull() : base(false) { }
        public override object ToValue(ScriptContext context) { return null; }

        public override bool BooleanValue { get { return false; } }

        public override string ToValueString(ScriptContext context) { return "null"; }

        public override string TypeName { get { return "null"; } }
    }

    public sealed class ScriptObject : ScriptObjectBase
    {
        [ObjectConstructor]
        public ScriptObject() { }

        public override object ToValue(ScriptContext context)
        {
            ScriptObjectConvertHandler handler = ScriptGlobal.ObjectConverter;
            if (handler != null) return handler(context, this);
            return this;
        }

        public override string ToValueString(ScriptContext context) { return "[Object]"; }
    }

    public interface IScriptFunction
    {
        IScriptObject Invoke(IScriptObject instance, IScriptObject[] argus);
    }

    public class FunctionInvoker
    {
        private ScriptFunctionBase func;
        private ScriptContext context;

        internal FunctionInvoker(ScriptContext context, ScriptFunctionBase func)
        {
            this.context = context;
            this.func = func;
        }

        public ScriptContext Context { get { return context; } }

        public ScriptFunctionBase Function { get { return func; } }

        public object Invoke(object[] argus)
        {
            IScriptObject[] scriptArgus = argus as IScriptObject[];
            if (scriptArgus == null && argus != null)
            {
                int length = argus.Length;
                if (length > 0)
                {
                    scriptArgus = new IScriptObject[length];
                    for (int i = 0; i < length; i++)
                        scriptArgus[i] = ScriptGlobal.ConvertValue(context, argus[i]);
                }
            }
            if (scriptArgus == null) scriptArgus = new IScriptObject[0];
            return func.Invoke(context, false, false, null, scriptArgus);
        }
    }

    public abstract class ScriptFunctionBase : ScriptObjectBase
    {
        protected ScriptFunctionBase() { }

        internal protected abstract IScriptObject Invoke(ScriptContext context, bool isScriptEnv, bool isNewObject, IScriptObject instance, IScriptObject[] argus);

        protected abstract ScriptFunctionBase OnBind(ScriptContext context, IScriptObject instance);

        public override object ToValue(ScriptContext context)
        {
            return new FunctionInvoker(context, this);
        }

        public override string ToValueString(ScriptContext context) { return "[Function]"; }

        public override string TypeName { get { return "function"; } }

        [ObjectMember("call")]
        internal IScriptObject Call(ScriptMethodArgus argus)
        {
            if (argus.Arguments.Length > 0)
            {
                IScriptObject instance = argus.Arguments[0];
                IScriptObject[] values;
                if (argus.Arguments.Length > 1)
                {
                    values = new IScriptObject[argus.Arguments.Length - 1];
                    Array.Copy(argus.Arguments, 1, values, 0, values.Length);
                }
                else values = new IScriptObject[0];
                return Invoke(argus.Context, true, false, instance, values);
            }
            throw new ScriptExecuteException("call方法必须传入对象参数。");
        }

        [ObjectMember("apply")]
        internal IScriptObject Apply(IScriptObject instance, IScriptObject argus, ScriptContext context)
        {
            if (instance != null)
            {
                IScriptObject[] values;
                IScriptArray array = argus as IScriptArray;
                if (array != null)
                {
                    int length = array != null ? array.ArrayLength : 0;
                    values = new IScriptObject[length];
                    for (int i = 0; i < length; i++)
                        values[i] = array.GetElementValue(context, i);
                }
                else values = new IScriptObject[0];
                return Invoke(context, true, false, instance, values);
            }
            throw new ScriptExecuteException("apply方法必须传入对象参数。");
        }

        [ObjectMember("bind")]
        internal IScriptObject Bind(IScriptObject instance, ScriptContext context)
        {
            return OnBind(context, instance);
        }
    }

    public sealed class ScriptFunction : ScriptFunctionBase
    {
        private ScriptExecuteContext parentContext;
        private DefineContext context;
        private IScriptObject instance;

        internal ScriptFunction(DefineContext context, ScriptExecuteContext parentContext)
        {
            this.context = context;
            this.parentContext = parentContext;
        }

        internal DefineContext Context { get { return context; } }

        protected internal override IScriptObject Invoke(ScriptContext scriptContext, bool isScriptEnv, bool isNewObject, IScriptObject instance, IScriptObject[] argus)
        {
            ScriptExecuteContext execContext = this.context.CreateExecuteContext(null, argus);
            execContext.Parent = parentContext;
            execContext.IsNewObject = isNewObject;
            if (isNewObject)
            {
                ScriptObject newObj = new ScriptObject();
                newObj.Parent = this.ProtoType;
                execContext.Instance = newObj;
            }
            else
            {
                if (this.instance != null) instance = this.instance;
                execContext.Instance = instance;
            }
            if (isScriptEnv)
            {
                scriptContext.SetInvokeContext(execContext);
                return null;
            }
            else
            {
                Thread current = Thread.CurrentThread;
                bool exchangeThread = scriptContext.executingThread == null;
                if (!exchangeThread && scriptContext.executingThread != current)
                    throw new ScriptExecuteException("同一个脚本上下文的方法，不允许多线程执行。");
                if (exchangeThread && Interlocked.CompareExchange<Thread>(ref scriptContext.executingThread, current, null) !=null)
                    throw new ScriptExecuteException("同一个脚本上下文的方法，不允许在多处同时执行。");
                int step = 0;
                ScriptExecuteContext oldRoot = null;
                try
                {
                    oldRoot = scriptContext.ResetRootContext(execContext);
                    step = 1;
                    scriptContext.PushContext(execContext);
                    step = 2;
                    ScriptParser.InternalExecute(scriptContext, execContext);
                }
                finally
                {
                    if (step >= 2) scriptContext.PopContext();
                    if (step >= 1) scriptContext.ResetRootContext(oldRoot);
                    if (exchangeThread)
                        Interlocked.CompareExchange<Thread>(ref scriptContext.executingThread, null, current);
                }
                return isNewObject ? execContext.Instance : execContext.Result;
            }
        }

        protected override ScriptFunctionBase OnBind(ScriptContext context, IScriptObject instance)
        {
            ScriptFunction result = new ScriptFunction(this.context, this.parentContext);
            result.instance = instance;
            return result;
        }
    }

    public interface IScriptArray
    {
        IScriptObject GetElementValue(ScriptContext context, int index);

        void SetElementValue(ScriptContext context, int index, IScriptObject value);

        int ArrayLength { get; }

        bool IsArray { get; }
    }
    
    public sealed class ScriptArray : ScriptObjectBase, IScriptArray
    {
        private IScriptObject[] list;
        private int listCount;
        private ScriptArrayItem[] otherList;
        private int otherCount;
        private int version;

        public ScriptArray() { }

        public ScriptArray(int length)
        {
            CheckListCapacity(length);
            listCount = length;
        }

        public ScriptArray(IScriptObject[] elements)
        {
            if (elements != null && elements.Length > 0)
            {
                list = elements;
                listCount = elements.Length;
            }
        }

        private int FindOther(int index)
        {
            if (otherCount > 0)
            {
                int l = 0, h = otherCount - 1, m, sub;
                do
                {
                    m = (l + h) >> 1;
                    sub = otherList[m].Index - index;
                    if (sub == 0) return m;
                    if (sub > 0) h = m - 1;
                    else l = m + 1;
                } while (l <= h);
                return ~l;
            }
            else return ~0;
        }

        private void CheckCapacity<T>(ref T[] array, ref int count, int increment)
        {
            if (array == null || count + increment > array.Length)
            {
                int newSize;
                if (count < 128)
                    newSize = ((increment >> 4) + 1) << 4;
                else if (count < 1024)
                    newSize = ((increment >> 8) + 1) << 8;
                else
                    newSize = ((increment >> 10) + 1) << 10;
                Array.Resize<T>(ref array, count + newSize);
            }
        }

        private void CheckListCapacity(int increment)
        {
            CheckCapacity<IScriptObject>(ref list, ref listCount, increment);
        }

        private void CheckOtherCapacity()
        {
            CheckCapacity<ScriptArrayItem>(ref otherList, ref otherCount, 1);
        }

        public IScriptObject this[int index]
        {
            get
            {
                IScriptObject result = null;
                if (index >= 0)
                {
                    if (index < listCount) result = list[index];
                    else if (otherCount > 0)
                    {
                        int i = FindOther(index);
                        if (i >= 0) result = otherList[i].Value;
                    }
                }
                if (result == null) result = ScriptUndefined.Instance;
                return result;
            }
            set
            {
                if (index >= 0)
                {
                    if (index < listCount) list[index] = value;
                    else if (index == listCount)
                    {
                        CheckListCapacity(1);
                        list[index] = value;
                        listCount++;
                        if (otherCount > 0 && otherList[0].Index == listCount)
                        {
                            int c = 1;
                            while (c < otherCount)
                            {
                                if (otherList[c].Index != listCount + c) break;
                                c++;
                            }
                            CheckListCapacity(c);
                            for(int i = 0; i < c; i++)
                                list[listCount + i] = otherList[i].Value;
                            listCount += c;
                            if (c < otherCount)
                                Array.Copy(otherList, c, otherList, 0, otherCount - c);
                            otherCount = c;
                        }
                    }
                    else
                    {
                        int i = FindOther(index);
                        if (i < 0)
                        {
                            i = ~i;
                            CheckOtherCapacity();
                            if (i < otherCount)
                                Array.Copy(otherList, i, otherList, i + 1, otherCount - i);
                            otherCount++;
                            otherList[i].Index = index;
                        }
                        otherList[i].Value = value;
                    }
                    version++;
                }
            }
        }

        public override IEnumerator GetEnumerator(ScriptContext context, bool isKey)
        {
            int oldVersion = version;
            for (int i = 0; i < listCount; i++)
            {
                if (oldVersion != version)
                    throw new ScriptExecuteException("数组枚举期间，不能添加或删除数组的成员。");
                if (isKey) yield return i;
                else yield return list[i];
            }
            for (int i = 0; i < otherCount; i++)
            {
                if (oldVersion != version)
                    throw new ScriptExecuteException("数组枚举期间，不能添加或删除数组的成员。");
                if (isKey) yield return otherList[i].Index;
                else yield return otherList[i].Value;
            }
            IEnumerator en = base.GetEnumerator(context, isKey);
            while (en.MoveNext())
                yield return en.Current;
        }

        public IScriptObject GetElementValue(ScriptContext context, int index)
        {
            return this[index];
        }

        public void SetElementValue(ScriptContext context, int index, IScriptObject value)
        {
            this[index] = value;
        }

        public int ArrayLength
        {
            get
            {
                if (otherCount > 0) return otherList[otherCount - 1].Index + 1;
                return listCount;
            }
        }

        public bool IsArray { get { return true; } }

        public override string ToValueString(ScriptContext context) { return "[Array]"; }

        public override object ToValue(ScriptContext context)
        {
            int len = Length;
            object[] result = new object[len];
            if (listCount > 0)
            {
                for (int i = 0; i < listCount; i++)
                    result[i] = list[i].ToValue(context);
            }
            if (otherCount > 0)
            {
                for (int i = 0; i < otherCount; i++)
                    result[otherList[i].Index] = otherList[i].Value.ToValue(context);
            }
            return result;
        }

        #region 脚本方法/属性

        [ObjectConstructor]
        public static ScriptArray CreateInstance(ScriptMethodArgus argus)
        {
            IScriptObject[] objArgus = argus.Arguments;
            if (objArgus != null && objArgus.Length > 0)
            {
                if (objArgus.Length == 1 && objArgus[0] is ScriptInteger)
                {
                    long value = ((ScriptInteger)objArgus[0]).IntegerValue;
                    return new ScriptArray((int)value);
                }
                return new ScriptArray(objArgus);
            }
            return new ScriptArray();
        }

        [ObjectMember("push")]
        public void Push(IScriptObject item)
        {
            if (otherCount > 0)
            {
                CheckOtherCapacity();
                ScriptArrayItem arrayItem = new ScriptArrayItem();
                arrayItem.Index = otherList[otherCount - 1].Index + 1;
                arrayItem.Value = item;
                otherList[otherCount++] = arrayItem;
            }
            else
            {
                CheckListCapacity(1);
                list[listCount++] = item;
            }
            version++;
        }

        [ObjectMember("pop")]
        public IScriptObject Pop()
        {
            if (otherCount > 0)
            {
                IScriptObject result = otherList[--otherCount].Value;
                otherList[otherCount].Value = null;
                version++;
                return result;
            }
            else if (listCount > 0)
            {
                IScriptObject result = list[--listCount];
                list[listCount] = null;
                version++;
                return result;
            }
            return ScriptUndefined.Instance;
        }

        [ObjectMember("length")]
        public int Length { get { return ArrayLength; } }

        #endregion

        #region 内部类

        private struct ScriptArrayItem
        {
            public int Index;
            public IScriptObject Value;
        }
        
        #endregion
    }

    internal interface IScriptAssignObject
    {
        IScriptObject GetFieldValue(ScriptContext context);

        void SetFieldValue(ScriptContext context, IScriptObject value);

        IScriptObject GetFieldValue2(ScriptContext context);

        void RemoveField(ScriptContext context);
    }

    internal class ScriptArrayAssignObject : IScriptObject, IScriptAssignObject
    {
        private IScriptArray instance;
        private int index;

        public ScriptArrayAssignObject(IScriptArray instance, int key)
        {
            this.instance = instance;
            this.index = key;
        }

        public IScriptObject GetFieldValue(ScriptContext context)
        {
            return instance.GetElementValue(context, index);
        }

        public void SetFieldValue(ScriptContext context, IScriptObject value)
        {
            instance.SetElementValue(context, index, value);
        }

        public IScriptObject GetFieldValue2(ScriptContext context)
        {
            return instance.GetElementValue(context, index);
        }

        public void RemoveField(ScriptContext context)
        {
            ((IScriptObject)instance).Remove(context, index.ToString());
        }

        #region IScriptObject

        IScriptObject IScriptObject.GetValue(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        void IScriptObject.SetValue(ScriptContext context, string name, IScriptObject value)
        {
            throw new NotImplementedException();
        }

        bool IScriptObject.Remove(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        object IScriptObject.ToValue(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        string IScriptObject.ToValueString(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        string IScriptObject.TypeName { get { throw new NotImplementedException(); } }

        #endregion

        #region IScriptEnumerable

        IEnumerator IScriptEnumerable.GetEnumerator(ScriptContext context, bool isKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    internal class ScriptAssignObject : IScriptObject, IScriptAssignObject
    {
        private IScriptObject instance;
        private string field;
        private ScriptFieldInfo fieldInfo, fieldInfo2;

        public ScriptAssignObject(IScriptObject instance, string field)
        {
            this.instance = instance;
            this.field = field;
        }

        public ScriptAssignObject(IScriptObject value, ScriptFieldInfo fieldInfo)
        {
            this.instance = value;
            this.fieldInfo = fieldInfo;
        }

        public ScriptAssignObject(IScriptObject value, ScriptFieldInfo fieldInfo, ScriptFieldInfo fieldInfo2) : this(value, fieldInfo)
        {
            this.fieldInfo2 = fieldInfo2;
        }

        public IScriptObject Instance { get { return instance; } }

        public string Field { get { return field; } }

        internal ScriptFieldInfo FieldInfo { get { return fieldInfo; } }

        public IScriptObject GetFieldValue(ScriptContext context)
        {
            if (fieldInfo != null) return ((ScriptObjectBase)instance).GetValue(fieldInfo);
            return instance.GetValue(context, field);
        }

        public void SetFieldValue(ScriptContext context, IScriptObject value)
        {
            if (fieldInfo != null) ((ScriptObjectBase)instance).SetValue(fieldInfo, value);
            else instance.SetValue(context, field, value);
        }

        public IScriptObject GetFieldValue2(ScriptContext context)
        {
            if (fieldInfo2 != null) return ((ScriptObjectBase)instance).GetValue(fieldInfo2);
            return instance.GetValue(context, fieldInfo != null ? fieldInfo.FieldName : field);
        }

        public void RemoveField(ScriptContext context)
        {
            string name = field;
            if (name == null && fieldInfo != null) name = fieldInfo.FieldName;
            instance.Remove(context, name);
        }

        #region IScriptObject

        IScriptObject IScriptObject.GetValue(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        void IScriptObject.SetValue(ScriptContext context, string name, IScriptObject value)
        {
            throw new NotImplementedException();
        }

        bool IScriptObject.Remove(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        object IScriptObject.ToValue(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        string IScriptObject.ToValueString(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        string IScriptObject.TypeName { get { throw new NotImplementedException(); } }

        #endregion

        #region IScriptEnumerable

        IEnumerator IScriptEnumerable.GetEnumerator(ScriptContext context, bool isKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    internal class ScriptMemberProxy : IScriptObject
    {
        private IScriptObject instance;
        private IScriptObject member;

        public ScriptMemberProxy(IScriptObject instance, IScriptObject member)
        {
            this.instance = instance;
            this.member = member;
        }

        public IScriptObject Instance { get { return instance; } }

        public IScriptObject Member { get { return member; } }

        #region IScriptObject

        string IScriptObject.TypeName
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        IScriptObject IScriptObject.GetValue(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        bool IScriptObject.Remove(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        void IScriptObject.SetValue(ScriptContext context, string name, IScriptObject value)
        {
            throw new NotImplementedException();
        }

        object IScriptObject.ToValue(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        string IScriptObject.ToValueString(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IScriptEnumerable

        IEnumerator IScriptEnumerable.GetEnumerator(ScriptContext context, bool isKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    internal class ScriptObjectEnumerator : IScriptObject
    {
        private IEnumerator en;

        public ScriptObjectEnumerator(IEnumerator en) { this.en = en; }

        public bool MoveNext() { return en != null && en.MoveNext(); }

        public IScriptObject GetCurrentKey(ScriptContext context) { return ScriptGlobal.ConvertValue(context, en.Current); }

        #region IScriptObject

        IScriptObject IScriptObject.GetValue(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        void IScriptObject.SetValue(ScriptContext context, string name, IScriptObject value)
        {
            throw new NotImplementedException();
        }

        bool IScriptObject.Remove(ScriptContext context, string name)
        {
            throw new NotImplementedException();
        }

        object IScriptObject.ToValue(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        string IScriptObject.ToValueString(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        string IScriptObject.TypeName { get { throw new NotImplementedException(); } }

        #endregion

        #region IScriptEnumerable

        IEnumerator IScriptEnumerable.GetEnumerator(ScriptContext context, bool isKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
