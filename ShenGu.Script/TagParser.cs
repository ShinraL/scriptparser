﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShenGu.Script
{
    public class TagParser
    {
        private readonly static Dictionary<string, ParseNodeCallback> dicSysNodeParsers;
        private IScriptBuilder builder;
        private string text;
        private int index, offset, length;
        private Dictionary<string, ParseNodeCallback> dicNodeParsers;
        private TagNodeInfo stack, current;
        private readonly static CheckChildCallback switchChildCheck = new CheckChildCallback(CheckSwitchChildren);

        static TagParser()
        {
            dicSysNodeParsers = new Dictionary<string, ParseNodeCallback>(StringComparer.OrdinalIgnoreCase);
            dicSysNodeParsers.Add("if", new ParseNodeCallback(ParseIf));
            dicSysNodeParsers.Add("else", new ParseNodeCallback(ParseElse));
            dicSysNodeParsers.Add("for", new ParseNodeCallback(ParseFor));
            dicSysNodeParsers.Add("while", new ParseNodeCallback(ParseWhile));
            dicSysNodeParsers.Add("do", new ParseNodeCallback(ParseDoWhile));
            dicSysNodeParsers.Add("switch", new ParseNodeCallback(ParseSwitch));
            dicSysNodeParsers.Add("case", new ParseNodeCallback(ParseCase));
            dicSysNodeParsers.Add("default", new ParseNodeCallback(ParseDefault));
            dicSysNodeParsers.Add("break", new ParseNodeCallback(ParseBreak));
            dicSysNodeParsers.Add("continue", new ParseNodeCallback(ParseContinue));
            dicSysNodeParsers.Add("line", new ParseNodeCallback(ParseLine));
            dicSysNodeParsers.Add(string.Empty, new ParseNodeCallback(ParseCode));
            dicSysNodeParsers.Add("code", new ParseNodeCallback(ParseCode));
            dicSysNodeParsers.Add("try", new ParseNodeCallback(ParseTry));
            dicSysNodeParsers.Add("catch", new ParseNodeCallback(ParseCatch));
            dicSysNodeParsers.Add("finally", new ParseNodeCallback(ParseFinally));
        }

        #region 节点处理方法

        private static void AppendBrace(IScriptBuilder builder, bool isStarted)
        {
            builder.AppendScript(isStarted ? "\r\n{\r\n" : "\r\n}\r\n");
        }

        private static void ThrowAttrNotExist(TagParser parser, string tagName, string attrName)
        {
            parser.ThrowError("“{0}”节点必须包含“{1}”属性。", tagName, attrName);
        }

        private static string CheckGetAttrValue(TagParser parser, TagNodeInfo info, string tagName, string attrName)
        {
            string code = info.GetAttrValue(attrName);
            if (string.IsNullOrEmpty(code))
                ThrowAttrNotExist(parser, tagName, attrName);
            return code;
        }

        private static void ParseIf(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                string code = CheckGetAttrValue(parser, info, "if", "code");
                builder.AppendScript(string.Format("if ({0})", code));
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
                AppendBrace(builder, false);
        }

        private static void ParseElse(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                TagNodeInfo prev = info.Previous;
                if (prev == null
                    || (prev.Name != "if" && prev.Name != "else")
                    || (prev.Name == "else" && prev.GetAttrValue("if") == null))
                    parser.ThrowError("“else”节点必须在“if”或“else if”节点之后。");
                string code = info.GetAttrValue("if");
                if (code == null)
                    builder.AppendScript("else");
                else
                    builder.AppendScript(string.Format("else if ({0})", code));
                AppendBrace(builder, true);
            }
            else
                AppendBrace(builder, false);
        }

        private static void ParseFor(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                string code = CheckGetAttrValue(parser, info, "for", "code");
                builder.AppendScript(string.Format("for ({0})", code));
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
                AppendBrace(builder, false);
        }

        private static void ParseWhile(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            string code = CheckGetAttrValue(parser, info, "while", "code");
            if (info.NodeType == TagNodeType.Element)
            {
                builder.AppendScript(string.Format("while ({0})", code));
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
                AppendBrace(builder, false);
        }

        private static void ParseDoWhile(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                builder.AppendScript("do");
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
            {
                string code = CheckGetAttrValue(parser, info, "do...while", "while");
                AppendBrace(builder, false);
                builder.AppendScript(string.Format("while({0});\r\n", code));
            }
        }

        private static void ParseSwitch(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                string code = CheckGetAttrValue(parser, info, "switch", "code");
                builder.AppendScript(string.Format("switch ({0})", code));
                AppendBrace(builder, true);
                info.CheckChildHandler = switchChildCheck;
            }
            else if (info.NodeType == TagNodeType.EndElement)
                AppendBrace(builder, false);
        }

        private static void CheckSwitchChildren(TagParser parser, IScriptBuilder builder, TagNodeInfo parent, TagNodeInfo child)
        {
            switch(child.NodeType)
            {
                case TagNodeType.Element:
                case TagNodeType.EndElement:
                case TagNodeType.WholeElement:
                    if (child.Name == "case" || child.Name == "default") return;
                    break;
            }
            parser.ThrowError("“switch”的一级子节点，必须是“case”节点");
        }

        private static void ParseCase(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.Parent == null || info.Parent.NodeType != TagNodeType.Element || info.Parent.Name != "switch")
                parser.ThrowError("“case”必须包含在“switch”内部。");
            switch (info.NodeType)
            {
                case TagNodeType.Element:
                    {
                        string code = CheckGetAttrValue(parser, info, "case", "code");
                        builder.AppendScript(string.Format("case {0}:", code));
                        AppendBrace(builder, true);
                        break;
                    }
                case TagNodeType.EndElement:
                    {
                        string str = info.GetAttrValue("ignoreBreak");
                        if (str == null || str.Trim() != "true")
                            builder.AppendScript("break;");
                        AppendBrace(builder, false);
                        break;
                    }
                case TagNodeType.WholeElement:
                    {
                        string code = CheckGetAttrValue(parser, info, "case", "code");
                        builder.AppendScript(string.Format("case {0}:\r\n", code));
                        break;
                    }
            }
        }

        private static void ParseDefault(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                if (info.Parent == null || info.Parent.NodeType != TagNodeType.Element || info.Parent.Name != "switch")
                    parser.ThrowError("“default”必须包含在“switch”内部。");
                builder.AppendScript("default:");
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
            {
                string str = info.GetAttrValue("ignoreBreak");
                if (str == null || str.Trim() != "true")
                    builder.AppendScript("break;");
                AppendBrace(builder, false);
            }
        }

        private static void ParseBreak(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element || info.NodeType == TagNodeType.WholeElement)
                builder.AppendScript("break;\r\n");
        }

        private static void ParseContinue(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element || info.NodeType == TagNodeType.WholeElement)
                builder.AppendScript("continue;\r\n");
        }

        private static void ParseLine(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element || info.NodeType == TagNodeType.WholeElement)
            {
                string code = CheckGetAttrValue(parser, info, "line", "code");
                builder.AppendScript(code);
                if (!code.EndsWith(";")) builder.AppendScript(";\r\n");
                else builder.AppendScript("\r\n");
            }
        }

        private static void ParseCode(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                string content = parser.ReadNodeContent();
                builder.AppendScript(content);
                builder.AppendScript("\r\n");
            }
        }

        private static void ParseTry(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                builder.AppendScript("try");
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
                AppendBrace(builder, false);
        }

        private static void ParseCatch(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                if (info.Previous == null || info.Parent.Name != "try")
                    parser.ThrowError("“catch”必须在“try”之后。");
                string varName = CheckGetAttrValue(parser, info, "catch", "var");
                builder.AppendScript(string.Format("catch ({0})", varName));
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
                AppendBrace(builder, false);
        }

        private static void ParseFinally(TagParser parser, IScriptBuilder builder, TagNodeInfo info)
        {
            if (info.NodeType == TagNodeType.Element)
            {
                if (info.Previous == null || (info.Parent.Name != "try" && info.Parent.Name != "catch"))
                    parser.ThrowError("“catch”必须在“try”或者“catch”之后。");
                builder.AppendScript("finally");
                AppendBrace(builder, true);
            }
            else if (info.NodeType == TagNodeType.EndElement)
                AppendBrace(builder, false);
        }

        #endregion

        #region 私有方法

        private static bool IsWhiteSpace(char ch)
        {
            switch(ch)
            {
                case ' ':
                case '\t':
                case 'r':
                case '\n':
                    return true;
            }
            return false;
        }

        private static bool IsNameChar(char ch)
        {
            return (ch >= 'a' && ch <= 'z')
                || (ch >= 'A' && ch <= 'Z')
                || ch == '-' || ch == '_' || ch == ':';
        }

        private void SetCurrent(TagNodeInfo info)
        {
            if (stack != null)
            {
                CheckChildCallback check = stack.CheckChildHandler;
                if (check != null) check(this, builder, stack, info);
            }
            info.Previous = current;
            info.Parent = stack;
            current = info;
        }

        private void PushCurrent(TagNodeInfo info)
        {
            if (stack != null)
            {
                CheckChildCallback check = stack.CheckChildHandler;
                if (check != null) check(this, builder, stack, info);
            }
            info.Previous = current;
            info.Parent = stack;
            current = null;
            stack = info;
        }

        private TagNodeInfo PopCurrent(string tagName)
        {
            if (stack != null && stack.NodeType == TagNodeType.Element && stack.Name == tagName)
            {
                stack.NodeType = TagNodeType.EndElement;
                current = stack;
                stack = stack.Parent;
                return current;
            }
            ThrowError("读取到不匹配的结束节点：" + tagName);
            return null;
        }

        private void ThrowError(string errorMsg, params object[] tempateArgus)
        {
            if (tempateArgus != null && tempateArgus.Length > 0) errorMsg = string.Format(errorMsg, tempateArgus);
            throw new Exception(errorMsg);
        }

        private int LocateName(int i)
        {
            for (int j = i; j < length; j++)
            {
                if (!IsNameChar(text[j])) return j - i;
            }
            return length - i;
        }

        private string ReadName(int i, int len)
        {
            return len == 0 ? string.Empty : text.Substring(i, len);
        }

        private int LocateStringValue(int i)
        {
            char ch = text[i];
            if (ch == '\'' || ch == '"')
            {
                for(int j = i + 1; j < length; j++)
                {
                    if (ch == text[j]) return j - i + 1;
                }
            }
            return -1;
        }

        private string ReadStringValue(int i, int len)
        {
            if (len == 2) return string.Empty;
            return text.Substring(i + 1, len - 2);
        }

        private void SkipWhiteSpace()
        {
            int len = text.Length;
            for(; offset < len; offset++)
            {
                if (!IsWhiteSpace(text[offset])) break;
            }
        }

        private void ParseText(bool isWhiteSpace)
        {
            if (index < offset)
            {
                if (!isWhiteSpace)
                {
                    TagNodeInfo info = new TagNodeInfo(TagNodeType.Text, null);
                    info.Index = index;
                    info.Length = offset - index;
                    SetCurrent(info);
                    builder.AppendScript(string.Format("{0}({1}, {2});\r\n", builder.OutputFunctionName, info.Index, info.Length));
                }
                index = offset;
            }
        }

        private void ParseNode(bool isEnd)
        {
            int nameLen = LocateName(offset);
            string name = ReadName(offset, nameLen);
            offset += nameLen;

            TagNodeInfo info = null;
            char ch;
            do
            {
                SkipWhiteSpace();
                if (offset >= length) ThrowError("节点“{0}”没有结束符", name);
                ch = text[offset];
                if (ch == '/')
                {
                    if (++offset >= length || text[offset] != '>' || isEnd) ThrowError("节点“{0}”中读取到错误的符号：{1}", name, ch);
                    offset++;
                    if (info == null)
                        info = new TagNodeInfo(TagNodeType.WholeElement, name);
                    else
                        info.NodeType = TagNodeType.WholeElement;
                    SetCurrent(info);
                    break;
                }
                else if (ch == '>')
                {
                    offset++;
                    if (isEnd)
                    {
                        if (info != null) ThrowError("结束节点“{0}”不允许存在属性", name);
                        info = PopCurrent(name);
                        break;
                    }
                    if (info == null)
                        info = new TagNodeInfo(TagNodeType.Element, name);
                    PushCurrent(info);
                    break;
                }
                int attrLen = LocateName(offset);
                if (attrLen == 0) ThrowError("节点“{0}”的属性中，存在特殊字符：{1}", name, ch);
                string attrName = ReadName(offset, attrLen);
                offset += attrLen;
                SkipWhiteSpace();
                if (offset >= length || text[offset] != '=') ThrowError("节点“{0}”读取不到属性“{1}”的值", name, attrName);
                offset++;
                SkipWhiteSpace();
                attrLen = LocateStringValue(offset);
                if (attrLen < 0) ThrowError("节点“{0}”属性“{1}”的值，必须以单引号或双引号开始。", name, attrName);
                string attrValue = ReadStringValue(offset, attrLen);
                offset += attrLen;
                if (info == null)
                    info = new TagNodeInfo(TagNodeType.Element, name);
                info.Attrs.Add(attrName, attrValue);
            } while (true);

            ParseNodeCallback callback;
            if ((dicNodeParsers != null && dicNodeParsers.TryGetValue(info.Name, out callback))
                || dicSysNodeParsers.TryGetValue(info.Name, out callback))
                callback(this, builder, info);
            index = offset;
        }

        private void ParseLine()
        {
            int firstIndex = offset;
            for(;offset < length; offset++)
            {
                char ch = text[offset];
                if (ch == '}')
                {
                    string script = ReadName(firstIndex, offset - firstIndex);
                    builder.AppendScript(string.Format("{0}({1});\r\n", builder.WriteFunctionName, script));
                    offset++;
                    break;
                }
            }
            index = offset;
        }

        #endregion

        #region 公共方法

        public void Register(string tagName, ParseNodeCallback callback)
        {
            if (dicNodeParsers == null) dicNodeParsers = new Dictionary<string, ParseNodeCallback>(StringComparer.OrdinalIgnoreCase);
            dicNodeParsers.Add(tagName, callback);
        }

        private void InternalParse(IScriptBuilder builder, string script)
        {
            this.builder = builder;
            this.text = script;

            index = 0;
            length = text.Length;
            int len = text.Length - 2;
            bool isWhitespace = true;
            for (offset = 0; offset < len; offset++)
            {
                char ch = text[offset];
                if (ch == '<')
                {
                    char ch1 = text[offset + 1];
                    if (ch1 == '@')
                    {
                        ParseText(isWhitespace);
                        isWhitespace = true;
                        offset += 2;
                        ParseNode(false);
                        continue;
                    }
                    else if (ch1 == '/' && text[offset + 2] == '@')
                    {
                        ParseText(isWhitespace);
                        isWhitespace = true;
                        offset += 3;
                        ParseNode(true);
                        continue;
                    }
                }
                else if (ch == '@' && text[offset + 1] == '{')
                {
                    ParseText(isWhitespace);
                    isWhitespace = true;
                    offset += 2;
                    ParseLine();
                    continue;
                }
                if (!IsWhiteSpace(ch)) isWhitespace = false;
            }
            offset = length;
            ParseText(isWhitespace);
        }

        public void Parse(IScriptBuilder builder, string script, bool doCheck)
        {
            InternalParse(builder, script);
            if (doCheck)
            {
                string str = builder.GetScript();
                ScriptParser.Parse(str);
            }
        }

        public string Parse(string script, string outputFuncName, string writeFuncName, bool doCheck)
        {
            StringScriptBuilder builder = new StringScriptBuilder(outputFuncName, writeFuncName);
            Parse(builder, script, doCheck);
            return builder.ToString();
        }

        public string ReadNodeContent()
        {
            TagNodeInfo info = stack;
            if (info != null)
            {
                int len = length - 5;
                int firstIndex = offset, lastIndex = offset;
                for (; offset < len; offset++)
                {
                    char ch = text[offset];
                    if (ch == '<' && text[offset + 1] == '/' && text[offset + 2] == '@')
                    {
                        lastIndex = offset;
                        offset += 3;
                        int nameLen = LocateName(offset);
                        string name = ReadName(offset, nameLen);
                        if (info.Name == name)
                        {
                            offset += nameLen;
                            if (offset < len && text[offset] == '>')
                            {
                                offset++;
                                PopCurrent(name);
                                return ReadName(firstIndex, lastIndex - firstIndex);
                            }
                        }
                    }
                }
                ThrowError("读取不到节点“{0}”对应的结束节点。", info.Name);
            }
            return null;
        }

        #endregion
    }

    public delegate void ParseNodeCallback(TagParser parser, IScriptBuilder builder, TagNodeInfo info);

    public delegate void CheckChildCallback(TagParser parser, IScriptBuilder builder, TagNodeInfo parent, TagNodeInfo child);

    public interface IScriptBuilder
    {
        void AppendScript(string script);

        string OutputFunctionName { get; }

        string WriteFunctionName { get; }

        string GetScript();
    }

    public class StringScriptBuilder : IScriptBuilder
    {
        private StringBuilder buffer;
        private string outputFunc, writeFunc;

        public StringScriptBuilder(string outputFuncName, string writeFuncName)
        {
            this.outputFunc = outputFuncName;
            this.writeFunc = writeFuncName;
            this.buffer = new StringBuilder(128);
        }

        public string OutputFunctionName { get { return outputFunc; } }

        public string WriteFunctionName { get { return writeFunc; } }

        public void AppendScript(string script)
        {
            this.buffer.Append(script);
        }

        public string GetScript() { return this.buffer.ToString(); }

        public override string ToString()
        {
            return GetScript();
        }
    }

    public enum TagNodeType
    {
        Element, EndElement, WholeElement, Text
    }

    public class TagNodeInfo
    {
        private TagNodeType nodeType;
        private string name;
        private Dictionary<string, string> attrs;
        private int index, length;
        private TagNodeInfo parent, previous;
        private CheckChildCallback checkChild;
        private int state;

        internal TagNodeInfo(TagNodeType type, string name)
        {
            this.nodeType = type;
            this.name = name;
        }

        public TagNodeType NodeType
        {
            get { return nodeType; }
            internal set { nodeType = value; }
        }

        public string Name { get { return name; } }

        public Dictionary<string, string> Attrs
        {
            get
            {
                if (attrs == null) attrs = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                return attrs;
            }
        }

        public string GetAttrValue(string name)
        {
            string result;
            if (attrs != null && attrs.TryGetValue(name, out result)) return result;
            return null;
        }

        public int Index
        {
            get { return index; }
            internal set { index = value; }
        }

        public int Length
        {
            get { return length; }
            internal set { length = value; }
        }

        public TagNodeInfo Parent
        {
            get { return parent; }
            internal set { parent = value; }
        }

        public TagNodeInfo Previous
        {
            get { return previous; }
            internal set { previous = value; }
        }

        public CheckChildCallback CheckChildHandler
        {
            get { return checkChild; }
            set { checkChild = value; }
        }

        public int Status
        {
            get { return state; }
            set { state = value; }
        }
    }
}
