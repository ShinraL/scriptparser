# 简介

ScriptParser是一款可以在.net的环境下，解析执行`javascript`语法的开源组件，其主要定位如下：
1. 在.net环境下，动态执行部分业务逻辑，以实现在不修改原生程序的基础上，动态的扩展一些功能。
2. 提供一套可以用`javascript`来控制逻辑的模板机制，以便在.net环境下，可以动态生成文本（比如：某些情况下，代替.aspx生成html页面等）

## 支持语法
目前`ECMAScript 5`的语法，除了`with语法、Property、正则表达式`等个别语法之外，其它的都有支持。
支持的预设类型：`Object、Array、Number、String、Date`，以及部分预设方法：`parseInt、parseFloat、stringify、eval`等。
支持对`DataTable`的访问。
支持开发者根据需要，添加需要在`javascript`中调用的类、对象、方法及属性。

## 其它
1. 由于`javascript`对象的特殊性，所有需要传入到`javascript`中去访问的对象、方法，都需要直接或间接的转化成继承于IScriptObject的对象。
> .net中的对象，如果没有特别处理，会使用`ScriptType`、`ScriptNativeObject`、`ScriptNativeArray`、`ScriptNativeFunction`进行包装后，供`javascript`的语句中调用。
2. 该组件的解析和执行过程，使用内存结构来模拟线程栈的操作，以避免解析到恶意嵌套的语法，引发堆栈溢出，从而导致进程中止。

# Demo
请参见ScriptTest项目

## JavaScript的解析过程
````C#
//解析javascript语法，创建ScriptParser
ScriptParser parser = ScriptParser.Parse("var i = 0; return i + inc;");

//创建执行上下文：ScriptContext
ScriptContext context = new ScriptContext();

//添加变量，以及方法映射
context.AddValue("inc", 100);

//执行
parser.Execute(context);

//获取执行结果
object result = context.Result.ToValue(context);
````

## 模板的解析过程
1. 创建一个包含output和write方法的类
````C#
class ScriptResult
{
    private string source;
    private StringBuilder builder;

    public ScriptResult(string source)
    {
        this.source = source;
        this.builder = new StringBuilder();
    }

    [ScriptMapping("output")]
    public void Output(int index, int length)
    {
        builder.Append(source.Substring(index, length));
    }

    [ScriptMapping("write")]
    public void Write(IScriptObject value, ScriptContext context)
    {
        builder.Append(value.ToValueString(context));
    }

    public override string ToString()
    {
        return builder.ToString();
    }
}
````
2. 生成可以执行的`javascript`，并将`ScriptResult`映射到`ScriptContext`中执行

````C#
//将包含<@XXX></@XXX>的标签，转成可以执行的javascript语法
TagParser reader = new TagParser();
string script = reader.Parse(text, "output", "write", true);

//解析javascript语法
ScriptParser parser = ScriptParser.Parse(script);

ScriptContext context = new ScriptContext();
//将output和write的方法映射添加到执行的上下文中
ScriptResult result = new ScriptResult(text);
context.AddMappings(result);

//执行javascript脚本
parser.Execute(context);

//读取结果
string content = result.ToString();
````

# QQ交流群
1. 792893075[![快速加群](https://pub.idqqimg.com/wpa/images/group.png)](https://jq.qq.com/?_wv=1027&k=DeLntNXe)